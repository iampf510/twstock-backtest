FROM python:3.7

ADD ./stock /home/stock/
ADD requirements.txt /home/stock/
WORKDIR /home/stock/
RUN pip3 install -r requirements.txt

EXPOSE 5000

#ENTRYPOINT ["gunicorn", "--log-level", "debug", "--reload", "-w", "5", "-b", "0.0.0.0:5000", "manage:app"]
ENTRYPOINT ["python", "manage.py"]
