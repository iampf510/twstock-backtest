import os
class Config:
    SQLALCHEMY_DATABASE_URI = f'sqlite:////{os.getcwd()}/test.db'
    SECRETE_KEY = 'secret_key'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
