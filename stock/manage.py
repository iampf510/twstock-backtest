import os

from flask import Flask
from flask_script import Manager, Server
from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap
from ext import db
from app.view import view
from app.api import api

import setting

app = Flask(__name__)
#app.config.from_object(setting.Config)
app.debug = True
app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:////{os.getcwd()}/test.db'
app.config['host'] = '0.0.0.0'
app.config['debug'] = True
app.register_blueprint(view)
app.register_blueprint(api, url_prefix='/api')

db = SQLAlchemy(app)
bootstrap = Bootstrap(app)

#manager = Manager(app)
#manager.add_command('runserver', Server(host='0.0.0.0'))

if __name__ == '__main__':
    #manager.run()
    app.run(host='0.0.0.0', debug=True)
