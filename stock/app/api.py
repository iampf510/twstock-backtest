from datetime import datetime, timedelta
import requests

from flask import Blueprint, render_template
from .models import LTF, FDMR, HD, BS
from .updateDB import getLargeTraderFuturesInfo, getFutDailyMarketReportInfo, getHistoryData
from ext import db

api = Blueprint('api',__name__)


