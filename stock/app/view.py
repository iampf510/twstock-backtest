from datetime import datetime

from flask import Blueprint, render_template, request
from .models import LTF, FDMR, HD, BS
from ext import db
from sqlalchemy import text

view = Blueprint('view',__name__)

@view.route('/')
def index():
    return render_template('index.html')

